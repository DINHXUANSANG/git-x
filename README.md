## Git Exercises


### Requirements:

- Install [git](https://www.atlassian.com/git/tutorials/install-git) on your machine 

- Install [git-flow-avh](https://danielkummer.github.io/git-flow-cheatsheet/)

	```$ brew install git-flow-avh```

- Config git user.email with your wizeline email (lower case)

	```$ git config --global user.email your-email@wizeline.com```

### Exercises:
- Setup repository

	```$ git clone git@github.com:wizeline/git-exercises.git```

	Note: This step requires a `SSH key` to be added to your Github account. If you get `Permission denied (publickey)` error, please follow [this guideline](https://help.github.com/en/articles/connecting-to-github-with-ssh) to add your `SSH Key`

	```$ cd git-exercises```

	```$ git flow init -d```

- Follow instructions in each `git*` folder

