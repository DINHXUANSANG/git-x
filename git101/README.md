## Exercise 101 
#### Git basic opration: `git add` `git status` `git commit`
#### Using [git flow](https://danielkummer.github.io/git-flow-cheatsheet/) to create and publish feature branch
----

1. Run init script

	```$ ./git101/setup101.sh```

2. Create new feature branch as instruction from init script.

	Or you can do

	```$ git checkout -b feature/git101/{username} develop```

3. Verify HEAD is pointing to feature/git101/{username}:

	```$ git status```

4. Add changes:

	```$ echo "hello world" >> changes/hello.txt```

5. Verify changes/hello.txt appears in the list of untracked files (Changes not staged for commit):

	```$ git status```

6. Add all changes to local git staging (Changes to be committed):

	```$ git add .```

	Or

	```$ git add changes/hello.txt```

7. Verify changes/hello.txt appears in the list of staged files:

	```$ git status```

8. Commit with meaningful comment:

	```$ git commit -m "feature/git101 git practice"```

9. Verify that your branch staging is empty:

 	```$ git status```

10. Publish your branch to remote origin on GitHub:

	```$ git flow feature publish git101/{username}```

	Or

	```$ git push --set-upstream origin feature/git101/{username}```

	After complete this, a slack-bot `git-x` will notify you on Slack if it has been done correctly. 
	The remote branch feature/git101 is deleted automatically by CircleCI.

11. After compleleted, you can move to git102. Also using following command  to clean local branch:

	```$ git checkout develop && git branch -D feature/git101/{username}```
