#!/bin/bash

EMAIL=$(git config user.email)
EMAIL=$(echo $EMAIL | tr '[:upper:]' '[:lower:]')
USER=$(echo $EMAIL | sed -e "s/@wizeline.com//g")
GIT_BRANCH="git101/$USER"

git flow init -d > /dev/null 2>&1 # init git flow in case not inited yet
git checkout develop 
git fetch -p > /dev/null 2>&1 && git pull > /dev/null 2>&1
git reset origin/develop --hard

git branch -D "feature/$GIT_BRANCH" > /dev/null 2>&1
git push --delete origin feature/$GIT_BRANCH > /dev/null 2>&1

git config gitflow.feature.start.fetch yes

echo "Your USER NAME is: $USER"
echo "To start next step, RUN: git flow feature start ${GIT_BRANCH}"
