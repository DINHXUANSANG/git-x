## Exercise 102
#### Git rebase interactive, GitHub Pull Request, PR Check, PR Review, PR Merge
----

1. Run init script

	```$ ./git102/setup102.sh```

2. Now you are on a new created branch `feature/git102/{username}`. 
	Run `git log` and you will see your current branch with 2 commits ahead `develop`

3. You need to [rebase](https://www.atlassian.com/git/tutorials/rewriting-history#git-rebase-i) to combine 2 commits into one with proper comment:

	```$ git rebase -i develop```

	`reword` commit on the first line and change commit message to "feature/git102 rebase practice" and `fixup` commit on the second line

	The combined commit should have comment:

	`"feature/git102 rebase practice"`

4. Publish changes to remote:

	```$ git flow feature publish git102/{username}```

 	If remote branch exists, use `git push -f` to update the changes

5. Create Pull Request on Github from your branch to `develop` using the URL from previous command.

6. Add PR reviewer and submit PR. 
	
	It'll take a few seconds for CircleCI job to validate your PR.

	SlackBot will notify on Slack the result. You need to request someone to approve your PR.

7. Merge PR on GitHub using option 'Merge Pull Request'

8. Delete local and remote feature branch to complete exercise.

	git checkout develop && git pull origin develop
	git flow feature finish git102/{username} --fetch --no-ff --force_delete

	Or

	git branch -D feature/git102/{username} && git push --delete origin feature/git102/{username} 
