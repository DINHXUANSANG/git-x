#!/bin/bash

EMAIL=$(git config user.email)
EMAIL=$(echo $EMAIL | tr '[:upper:]' '[:lower:]')
USER=$(echo $EMAIL | sed -e "s/@wizeline.com//g")

git checkout develop
git fetch -p > /dev/null 2>&1 && git pull > /dev/null 2>&1
git reset origin/develop --hard
git branch -D feature/git102/$USER > /dev/null 2>&1 # remove branch on local in case rerun test
git push --delete origin feature/git102/$USER  > /dev/null 2>&1 #remove branch on remote in case rerun test

git config gitflow.feature.start.fetch yes
git flow feature start git102/$USER

echo "git 102" >> changes/"$USER".txt
git add changes/"$USER".txt && git commit -m 'adding new file'
echo "hello world" >> changes/"$USER".txt
git add changes/"$USER".txt && git commit -m 'adding content to file'

echo "You are on a new branch feature/git102/$USER with 2 commits added."
