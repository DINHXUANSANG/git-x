#!/bin/bash

last_commit_id=$(git log --format="%H" -n 1)
my_email=$(git log --format='%ae' ${last_commit_id}^! )
my_email=$(echo $my_email | tr '[:upper:]' '[:lower:]')
user=$(echo $my_email | sed -e "s/@wizeline.com//g")

send_slack() 
{   
    python scripts/slack.py $my_email "$1" "$2"
}

add_commit()
{
    echo "run demo" >> changes/$user.txt
    git add . && git commit -m 'add demorun'
    git push origin $CIRCLE_BRANCH
}

demo_flow=$1
echo $demo_flow

if [[ "$demo_flow" == "init" ]]; then
    add_commit # push a new commit to change history so that git history is different
    message=`cat templates/demo.txt`
    send_slack "$message" "format"
    git push origin --delete $CIRCLE_BRANCH
    exit 0
fi 

if [[ "$demo_flow" == "complete" ]]; then
    add_commit # push a new commit to change history so that git history is different
    message="The exercise is completed. The remote branch $CIRCLE_BRANCH is automatically deleted, you may try again if you like."
    send_slack "$message"
    git push origin --delete $CIRCLE_BRANCH
    # trigger kudos api
    curl -d "{\"email\":\"$my_email\"}" -H "x-api-key: $APIKEY" -X POST $URL
    exit 0
fi 
