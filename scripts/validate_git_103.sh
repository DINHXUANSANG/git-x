#!/bin/bash

LAST_COMMIT_ID=$(git log --format="%H" -n 1)
EMAIL=$(git log --format='%ae' ${LAST_COMMIT_ID}^! )
EMAIL=$(echo $EMAIL | tr '[:upper:]' '[:lower:]')

finish() 
{	
	echo $2
	python scripts/slack.py $1 "$2"
}

NUMBER_COMMIT=$(git log --oneline $CIRCLE_BRANCH ^origin/develop | wc -l)
echo number commit $NUMBER_COMMIT
if [[ "$NUMBER_COMMIT" != "1" ]]; then
	MESSAGE="Git 102 failed, there are more than 1 commit"
	finish $EMAIL "$MESSAGE"
	exit 1
fi

COMMENT=$(git log -1 --pretty=%B)
COMMENT=$(echo $COMMENT | xargs)
EXPECTED_COMMED="feature/git102 rebase practice"
echo comment content $COMMENT
if [[ "$COMMENT" != "$EXPECTED_COMMED" ]]; then
	MESSAGE="Git 102 failed, commit comment should be '$EXPECTED_COMMED'"
	finish $EMAIL "$MESSAGE"
	exit 1
fi

#validate changes content
CHANGES=$(git diff --name-only HEAD^ HEAD)
USER=$(echo $EMAIL | sed -e "s/@wizeline.com//g")
EXPECTED_CHANGES="changes/$USER.txt"
if [[ "$CHANGES" != "$EXPECTED_CHANGES" ]]; then
	MESSAGE="Git 102 failed, commit changes should be only file: '$EXPECTED_CHANGES'"
	finish $EMAIL "$MESSAGE"
	exit 1
fi 

MESSAGE="Congratulation your git102 branch has been built successfully. You can follow instructions to create a Pull Request and merge to develop branch."
finish $EMAIL "$MESSAGE"
exit 0
