# API slackClient v1: https://github.com/slackapi/python-slackclient/tree/v1
# API slackClient 2: https://slack.dev/python-slackclient/conversations.html#direct-messages
# Sending DM to slack user by bot: python slack.py $email $msg

import os
import sys

import slack

client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])
log_channel = os.environ.get('LOG_CHANNEL')  # current log channel #git-exercise-log
# client.chat_postMessage(channel="UF7MJ7XQA", text="hello", as_user=True) # testing

try:
    email = sys.argv[1]
    msg = sys.argv[2]
    username = email.replace("@wizeline.com", "")

    response = client.users_lookupByEmail(email=email)
    user = response.get('user')
    uid = user.get('id')
    if not user or not uid:
        raise Exception(f"User not found for email: {email}")

    msg = msg.replace("{username}", username)
    msg = msg.replace("{userid}", str(uid))

    client.chat_postMessage(channel=uid, text=msg, as_user=True)
    log_msg = f"sending msg to {email}"
    client.chat_postMessage(channel=log_channel, text=log_msg, as_user=True)
except Exception as e:
    client.chat_postMessage(channel=log_channel, text=str(e), as_user=True)
