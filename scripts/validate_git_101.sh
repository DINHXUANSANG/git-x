#!/bin/bash

LAST_COMMIT_ID=$(git log --format="%H" -n 1)
EMAIL=$(git log --format='%ae' ${LAST_COMMIT_ID}^! )
EMAIL=$(echo $EMAIL | tr '[:upper:]' '[:lower:]')

finish_git101() 
{	
	echo $2
	python scripts/slack.py $1 "$2"
	git push origin --delete $CIRCLE_BRANCH
	python scripts/slack.py $EMAIL "Remote branch origin $CIRCLE_BRANCH is now deleted"
}

NUMBER_COMMIT=$(git log --oneline $CIRCLE_BRANCH ^origin/develop | wc -l)
echo "number commit" $NUMBER_COMMIT
if [[ "$NUMBER_COMMIT" != "1" ]]; then
	MESSAGE="Git101 failed, there are $NUMBER_COMMIT commits, it should be one"
	finish_git101 $EMAIL "$MESSAGE"
	exit 1
fi

COMMENT=$(git log -1 --pretty=%B)
COMMENT=$(echo $COMMENT | xargs)
EXPECTED_COMMED="feature/git101 git practice"
echo "comment content" $COMMENT 
if [[ "$COMMENT" != "$EXPECTED_COMMED" ]]; then
	MESSAGE="Git 102 failed, commit comment should be '$EXPECTED_COMMED'"
	finish $EMAIL "$MESSAGE"
	exit 1
fi 

MESSAGE="Congratulation you have completed git 101"
finish_git101 $EMAIL "$MESSAGE"
exit 0
