#!/bin/bash

# git clone git@github.com:wizeline/git-exercises.git
# cd git-exercises

# Moving branch from bitbucket repo to GitHub
echo 'initting demo...'

git checkout develop > /dev/null 2>&1
git fetch -p > /dev/null 2>&1 && git pull > /dev/null 2>&1
git reset origin/develop --hard > /dev/null 2>&1

# add remote
git remote add bitrepo https://DINHXUANSANG@bitbucket.org/DINHXUANSANG/git-x.git
git fetch bitrepo > /dev/null 2>&1

my_email=$(git config user.email)
my_email=$(echo $my_email | tr '[:upper:]' '[:lower:]')
my_user=$(echo $my_email | sed -e "s/@wizeline.com//g")
my_branch=init_demo/$my_user

git checkout -b $my_branch > /dev/null 2>&1
echo "init demo" >> changes/note.txt
git add changes/note.txt && git commit -m "adding note"
git push --set-upstream origin $my_branch > /dev/null 2>&1

git checkout develop > /dev/null 2>&1
git branch -D $my_branch > /dev/null 2>&1

