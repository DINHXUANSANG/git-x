## Git Demo


### Instructions:

- Setup repository

    ```$ git clone git@github.com:wizeline/git-exercises.git```

    Note: This step requires a `SSH key` to be added to your Github account. If you get `Permission denied (publickey)` error, please follow [this guideline](https://help.github.com/en/articles/connecting-to-github-with-ssh) to add your `SSH Key`

    ```$ cd git-exercises```


- Config git user.email with your wizeline email (lower case)

	```git config --global user.email your-email@wizeline.com```

- Login slack application with Wizeline account

- Run init script
    ```sh demo/init.sh```


- Follow instructions from SlackBot
